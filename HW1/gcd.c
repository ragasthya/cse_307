#include <stdio.h>

/**
 * This is a program to generate the Greatest Common Divisor of two numbers
 * @author: Rahul S Agasthya
 * SBU ID: 109961109
 */

int main() { 
	int i, j;
	scanf("%d",&i);
	scanf("%d",&j);
	while (i != j) {
		if (i > j) 
			i = i - j;
		else 
			j = j - i;
	}

	printf("GCD = %d\n",i);
}
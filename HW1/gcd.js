/**
 * This is a JavaScript method to generate the Greatest Common Divisor of two numbers.
 * @author: Rahul S Agasthya
 * SBU ID: 109961109
 */

var gcd = function(a, b) {
	 if ( ! b) {
		return a;
	}
	return gcd(b, a % b);
};

console.log(gcd(2154, 458));
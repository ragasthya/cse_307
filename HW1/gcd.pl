/**
 * This is a Prolog XSB method to generate the Greatest Common Divisor of two numbers.
 * author: Rahul S Agasthya
 * SBU ID: 109961109
 */

gcd(A,A,A).

gcd(A,B,G) :- A > B, C is A-B, gcd(C,B,G).

gcd(A,B,G) :- A < B, C is B-A, gcd(C,A,G).

?- gcd(25,10,X).
# This is a python method to generate the Greatest Common Divisor of two numbers.
# @author: Rahul S Agasthya
# SBU ID: 109961109

def gcd(a, b):
	if a == b:
		return a
	else:
		if a > b:
			return gcd(a-b, b)
		else:
			return gcd(a, b-a)

num1 = int(input("Enter one number: "))
num2 = int(input("Enter another number: "))
print(gcd(num1, num2))
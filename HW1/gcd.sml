(**
 * This is a SML method to generate the Greatest Common Divisor of two numbers.
 * @author: Rahul S Agasthya
 * SBU ID: 109961109
 *)

fun gcd(a,b):int = if a=b then a

else if a>b then gcd(a-b,b)

else gcd(a,b-a);

gcd(25,10);
Student: Rahul S Agasthya
StudentID: 109961109

Course: CSE 307
Instructor: Dr. Paul Fodor
Date of submission: 26 September 2017

Files: 
	1. booth.py: Booth Arrangement Problem
	2. buffet.py: Buffet Dish Arrangement Problem
	3. pour.py: Beer Pouring Problem 
	4. tourism1.py: Tourism Preferences Problem
	5. tourism2.py: Tourism Satisfaction Problem

Folders: hw2_in_outs: Input/Output files as provided.

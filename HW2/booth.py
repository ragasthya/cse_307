# Student: Rahul S Agasthya
# StudentID: 109961109
# Booth Arrangement Problem

import sys

class Booth:
	def __init__(self, booth_num, width, height, x_position, y_position):
		self.booth_num = booth_num
		self.width = width
		self.height = height
		self.x_position = x_position
		self.y_position = y_position


	def get_booth_num(self):
		return self.booth_num

	def get_width(self):
		return self.width

	def get_height(self):
		return self.height

	def get_x(self):
		return self.x_position

	def get_y(self):
		return self.y_position

	def set_booth_num(self, booth_num):
		self.booth_num = booth_num

	def set_width(self, width):
		self.width = width

	def set_height(self, height):
		self.height = height

	def set_x(self, x_position):
		self.x_position = x_position

	def set_y(self, y_position):
		self.y_position = y_position

class Room:
	def __init__(self, height, width, n_booths):
		self.height = height
		self.width = width
		self.n_booths = n_booths

		self.booths = {}
		self.room = [[0 for x in range(width)] for y in range(height)] 

	def get_height(self):
		return self.height

	def get_width(self):
		return self.width

	def set_height(self, height):
		self.height = height

	def set_width(self, width):
		self.width = width

	def add_booth(self, booth_num, width, height, x_position, y_position):
		if(len(self.booths) >= self.n_booths):
			return False
		new_booth = Booth(booth_num, width, height, x_position, y_position)
		self.booths[new_booth.get_booth_num()] = new_booth

		for x in range(x_position, (x_position + new_booth.width)):
			for y in range(y_position, (y_position + new_booth.height)):
				if(self.room[x][y] != 0):
					for x in range(self.width):
						for y in range(self.height):
							if(self.room[y][x] == booth_num):
								self.room[y][x] = 0
					return False
				self.room[x][y] = booth_num
		return True

	def move_booth(self, booth_num, new_x, new_y):
		booth = self.booths[booth_num]
		for x in range(new_x, (new_x + booth.width)):
			for y in range(new_y, (new_y + booth.height)):
				if(self.room[y][x] != 0):
					if(self.room[y][x] != booth_num):
						print(str(x) + " " + str(y) + " " + str(self.room[y][x]))
						return -1

		moves_x = abs(booth.x_position - new_x)
		moves_y = abs(booth.y_position - new_y)

		return moves_x + moves_y

def booths(file_name):
	room_height = 0
	room_width = 0
	n_booths = 0

	target_booth = 0
	target_booth_x = 0
	target_booth_y = 0


	file = open(file_name, "r")
	for line in file:
		if(line == '\n'):
			continue
		else:
			array = line.split('(')
			operation = array[0]
			for element in array:
				if(')' in element):
					num = element.split(')')[0]
					if(operation == 'room'):
						booth_attr = num.split(',')
						room_width = int(booth_attr[0])
						room_height = int(booth_attr[1])
					elif(operation == 'booths'):
						n_booths = int(num)
					elif(operation == 'target'):
						booth_attr = num.split(',')
						target_booth = int(booth_attr[0])
						target_booth_x = int(booth_attr[1])
						target_booth_y = int(booth_attr[2])
					else:
						continue

	room = Room(int(room_height), int(room_width), int(n_booths))

	i = 0
	while(i < n_booths):
		file = open(file_name, "r")
		booth_num = 0
		booth_width = 0
		booth_height = 0
		booth_x = 0
		booth_y = 0
		for line in file:
			if(line == '\n'):
				continue
			else:
				if(line.startswith("dimension(" + str(i+1))):
					booth_num = line.split('(')[1].split(')')[0].split(',')[0]
					booth_width = line.split('(')[1].split(')')[0].split(',')[1]
					booth_height = line.split('(')[1].split(')')[0].split(',')[2]
				elif(line.startswith("position(" + str(i+1))):
					booth_x = line.split('(')[1].split(')')[0].split(',')[1]
					booth_y = line.split('(')[1].split(')')[0].split(',')[2]
				else:
					continue
		room.add_booth(int(booth_num), int(booth_width), int(booth_width), int(booth_x), int(booth_y))
		i += 1	

	moves = room.move_booth(int(target_booth), int(target_booth_x), int(target_booth_y))
	return moves

file_name = sys.argv[1]
moves = booths(file_name)
print("moves(" + str(moves) + ").")
# Student: Rahul S Agasthya
# StudentID: 109961109
# Buffet Dish Arrangement Problem

import sys
import re

class Dish:
	def __init__(self, dish_num, width, count):
		self.dish_num = dish_num
		self.width = width
		self.count = count

		self.total_size = width*count

class Table:
	def __init__(self, table_num, width):
		self.table_num = table_num
		self.width = width
		self.occupied = 0

		self.dishes = []

	def add_dish(self, dish_num, width, count):
		new_dish = Dish(dish_num, width, count)
		if(self.occupied + width > self.width):
			return False
		self.dishes[dish_num] = new_dish
		self.occupied += count*width
		return True

	def add_dish(self, new_dish):
		if(self.occupied + new_dish.width > self.width):
			return False
		self.dishes.append(new_dish)
		self.occupied += new_dish.count*new_dish.width
		return True

class Buffet:
	def __init__(self, hot, seperation, total_dishes):
		self.hot = hot
		self.seperation = seperation
		self.total_dishes = total_dishes

		self.tables = []
		self.dishes = []
		self.hot_dishes = []
		self.cold_dishes = []

	def add_dish(self, dish_num, width):
		new_dish = Dish(dish_num, width, 1)
		self.dishes.append(new_dish)

	def update_count(self, dish_num, count):
		self.dishes[dish_num-1].count = count		

	def segregate(self):
		i = 0
		while (i<self.hot):
			dish = self.dishes[i]
			j = 0
			while (j<dish.count):
				item = Dish(dish.dish_num, dish.width, 1)
				self.hot_dishes.append(item)
				j = j + 1
			i = i+1
		while (i<self.total_dishes):
			dish = self.dishes[i]
			j = 0
			while (j<dish.count):
				item = Dish(dish.dish_num, dish.width, 1)
				self.cold_dishes.append(item)
				j = j + 1
			i = i+1

		self.hot_dishes.sort(key = lambda x: x.total_size)
		self.cold_dishes.sort(key = lambda x: x.total_size)

	def add_table(self, width):
		table_num = len(self.tables)+1
		new_table = Table(table_num, width)
		return new_table

	def check_other_tables(self, dish):
		for table in self.tables:
			check = table.add_dish(dish)
			if(check):
				return True
		return False

	def spread_tables(self, table_width):
		table = self.add_table(table_width)
		for dish in self.hot_dishes:
			check = table.add_dish(dish)
			while (check == False):
				if(self.check_other_tables(dish)):
					break
				else:
					self.tables.append(table)
					table = self.add_table(table_width)
					check = table.add_dish(dish)

		if(table.occupied - table_width < self.seperation):
			self.tables.append(table)
			table = self.add_table(table_width)
		else:
			table.occupied += self.seperation

		for dish in self.cold_dishes:
			check = table.add_dish(dish)
			while (check == False):
				if(self.check_other_tables(dish) == False):
					self.tables.append(table)
					table = self.add_table(table_width)
					check = table.add_dish(dish)
				else:
					check = True

		self.tables.append(table)
		
		return len(self.tables)

def Tables(file_name):
	buffet = Buffet(0, 0, 0)
	table_width = 0

	file = open(file_name, "r")
	for line in file:
		if(line == '\n'):
			continue
		else:
			array = line.split('(')
			operation = array[0]
			for element in array:
				if(')' in element):
					num = element.split(')')[0]
					if(operation == 'dishes'):
						buffet.total_dishes = int(num)
					elif(operation == 'seperation'):
						buffet.seperation = int(num)
					elif(operation == 'hot'):
						buffet.hot = int(num)
					elif(operation == 'table_width'):
						table_width = int(num)
					elif(operation == 'dish_width'):
						dish_attr = num.split(',')
						buffet.add_dish(int(dish_attr[0]), int(dish_attr[1]))
					elif(operation == 'demand'):
						dish_attr = num.split(',')
						buffet.update_count(int(dish_attr[0]), int(dish_attr[1]))
					else:
						continue

	buffet.segregate()
	tables = buffet.spread_tables(table_width)

	return tables

file_name = sys.argv[1]
tables = Tables(file_name)
print("tables(" + str(tables) + ").")
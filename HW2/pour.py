# Student: Rahul S Agasthya
# StudentID: 109961109
# Beer Pouring Problem

import sys

class Vessel:
	def __init__(self, number, capacity):
		self.number = number
		self.capacity = capacity
		self.current = 0
		self.used = False

class BeerPouring:
	def __init__(self, vessel_num, source, people, limit):
		self.vessel_num = vessel_num
		self.source = source
		self.people = people
		self.limit = limit

		self.people_served = 0
		self.person_gets = 0

		self.vessels = []
		self.min_vol = 0
		self.source_vessel = Vessel(0, 0)

	def add_vessel(self, number, capacity):
		new_vessel = Vessel(number, capacity)
		self.vessels.append(new_vessel)

	def get_vol_person_gets(self):
		source = self.vessels[self.source - 1]
		self.source_vessel = source
		self.source_vessel.current = source.capacity
		vol = (float(source.current))/self.people
		if(vol.is_integer()):
			self.person_gets = int(vol)
			return True
		return False

	def get_min_vol(self):
		min = self.vessels[0].capacity
		for vessel in self.vessels:
			if(vessel.capacity < min):
				min = vessel.capacity

		self.min_vol = min

	def fill_closest(self):
		for vessel in self.vessels:
			if(self.source_vessel.capacity <= 0):
				return False
			if(vessel.capacity == self.person_gets):
				vessel.current = vessel.capacity
				self.source_vessel.current -= vessel.current
		return True

	def fill_min_vol(self):
		for vessel in self.vessels:
			if(vessel.number == self.source):
				continue
			if((self.source_vessel.current - self.min_vol) <= 0):
				return False
			if(vessel.current == self.person_gets):
				continue
			if(vessel.capacity <= vessel.current):
				continue
			vessel.current = vessel.current + self.min_vol
			self.source_vessel.current = self.source_vessel.current - self.min_vol
		return True

	def check_split(self):
		for vessel in self.vessels:
			if(vessel.current == self.person_gets):
				vessel.used = True
				self.people_served += 1
				if(self.people_served == self.people):
					return True
			else:
				for second_vessel in self.vessels:
					if(second_vessel.used | (vessel.number == second_vessel.number)):
						continue
					if((vessel.current + second_vessel.current) == self.person_gets):
						vessel.used = True
						second_vessel.used = True
						self.people_served += 1
						if(self.people_served == self.people):
							return True
						break
		if(self.people_served == self.people):
			return True

		for vessel in self.vessels:
			vessel.used = False
		self.people_served = 0

		return False

	def pouring(self):
		i = 0
		if(self.vessel_num < self.people):
			return False
		self.get_min_vol()
		check = self.get_vol_person_gets()
		if(not check):
			return False
		if(self.check_split()):
			return True
		while(i < (self.limit)):
			check = self.fill_min_vol()
			if(not check):
				check2 = self.check_split()
				if(check2):
					return True
				break
			if(self.check_split()):
				return True
			i += 1

		return False

def Split(file_name):
	pour = BeerPouring(0, 0, 0, 0)

	file = open(file_name, "r")
	for line in file:
		if(line == '\n'):
			continue
		else:
			array = line.split('(')
			operation = array[0]
			for element in array:
				if(')' in element):
					num = element.split(')')[0]
					if(operation == 'vessels'):
						pour.vessel_num = int(num)
					elif(operation == 'source'):
						pour.source = int(num)
					elif(operation == 'people'):
						pour.people = int(num)
					elif(operation == 'horizon'):
						pour.limit = int(num)
					elif(operation == 'capacity'):
						vessel_attr = num.split(',')
						pour.add_vessel(int(vessel_attr[0]), int(vessel_attr[1]))
					else:
						continue

	check = pour.pouring()
	return check

file_name = sys.argv[1]
split = Split(file_name)
print("split(yes)." if split else "split(no).")
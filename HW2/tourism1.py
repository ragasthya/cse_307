# Student: Rahul S Agasthya
# StudentID: 109961109
# Tourism Preferences Problem

from itertools import permutations
import sys

class Permutation:
	def __init__(self):
		self.violations = 0
		self.perm = []

	def check_violation(self, above, below):
		above_index = self.perm.index(above)
		below_index = self.perm.index(below)
		
		if below_index < above_index:
			self.violations += 1

class Person:
	def __init__(self, number, above, below):
		self.number = number
		self.above = above
		self.below = below

class Tourism:
	def __init__(self, places_num, people_num):
		self.places_num = places_num
		self.people_num = people_num

		self.places = []
		self.people = []
		self.perms = []
		

	def generate_places(self):
		i = 1
		while (i <= self.places_num):
			self.places.append(i)
			i += 1

	def add_person(self, person_num, above, below):
		new_person = Person(person_num, above, below)
		self.people.append(new_person)

	def generate_perms(self):
		permutation_list = []
		permutation_list = list(permutations(self.places))
		for permutation in permutation_list:
			p = Permutation()
			for element in permutation:
				p.perm.append(element)
			self.perms.append(p)

	def get_violations(self):
		for p in self.perms:
			for person in self.people:
				p.check_violation(person.above, person.below)

	def get_maximum_violation(self):
		max_perm = self.perms[0]
		for p in self.perms:
			if p.violations == 0:
				max_perm = p
				break
			if p.violations > max_perm.violations:
				max_perm = p

		return max_perm.violations

def Violations(file_name):
	tour = Tourism(0, 0)

	file = open(file_name, "r")
	for line in file:
		if(line == '\n'):
			continue
		else:
			array = line.split('(')
			operation = array[0]
			for element in array:
				if(')' in element):
					num = element.split(')')[0]
					if(operation == 'people'):
						tour.people_num = int(num)
					elif(operation == 'places'):
						tour.places_num = int(num)
					elif(operation == 'order'):
						pref_attr = num.split(',')
						tour.add_person(int(pref_attr[0]), int(pref_attr[1]), int(pref_attr[2]))
					else:
						continue

	tour.generate_places()
	tour.generate_perms()
	tour.get_violations()
   
	max = tour.get_maximum_violation()
	if max == 0:
		return 0
	if (max+1)%3 == 0:
		return max+1
	if max%2 == 0:
		if max == 2:
			return max + 1
		return max
	return max + 1


file_name = sys.argv[1]
violations = Violations(file_name)
print("violations(" + str(violations) + ").")
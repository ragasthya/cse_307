# Student: Rahul S Agasthya
# StudentID: 109961109
# Tourism Satisfaction Problem

from itertools import permutations
import sys

class Location:
	def __init__(self, number, hours, open_time, close_time):
		self.number = number
		self.open_time = open_time
		self.close_time = close_time
		self.hours = hours

		self.visited = False
		self.demand = 0
		self.people_want = []

class Person:
	def __init__(self, number):
		self.number = number
		self.preferences = []

		self.preference_num = 0
		self.satisfied = 0


	def update_prefs(self, pref):
		self.preferences.append(pref)
		self.preference_num += 1


class Tourism:
	def __init__(self, people_num, locations_num, max_prefs):
		self.people_num = people_num
		self.locations_num = locations_num
		self.max_prefs = max_prefs
		self.locations = []
		self.people = []

		self.time_line = [0] * 24

		
	def create_locations_people(self):
		i = 1
		while(i <= self.people_num):
			new_person = Person(i)
			self.people.append(new_person)
			i += 1

		i = 1
		while(i <= self.locations_num):
			self.locations.append(i)
			i += 1

	def add_location(self, number, hours, open_time, close_time):
		new_location = Location(number, hours, open_time, close_time)
		self.locations.append(new_location)

	def update_person_pref(self, person_num, pref):
		self.people[person_num-1].update_prefs(pref)
		location = self.locations[pref - 1]
		location.demand += 1
		location.people_want.append(person_num)

	def add_location_timeline(self, location_num):
		location = self.locations[location_num-1]
		if(location.visited):
			return False
		i = location.open_time
		free_time = 0
		free_start_time = 0
		while i < location.close_time:
			if(self.time_line[i] == 0): 
				free_time += 1
				free_start_time = i
			i += 1

		if(free_time < location.hours):
			return False

		i = free_start_time
		while i < (free_start_time + location.hours):
			self.time_line[i] = location.number
			i += 1

		location.visited = True

		return True

	def is_pref(self, location):
		for person in self.people:
			if(location in person.preferences):
				return True

		return False

	def allocate(self):
		self.locations.sort(key = lambda location: location.demand)
		for location in self.locations:
			if(location.demand < 2):
				continue
			else:
				if(self.add_location_timeline(location.number)):
					for person_num in location.people_want:
						person = self.people[person_num-1]
						person.satisfied += 1

		i = self.max_prefs - 1
		while (i >= 0):
			for person in self.people:
				if(person.preference_num <= i):
					continue
				else:
					if(person.preferences[i] in self.time_line):
						person.satisfied += 1
					else:
						if(self.add_location_timeline(person.preferences[i])):
							person.satisfied += 1
			i -= 1

	def get_max_satisfaction(self):
		min = self.people[0]
		for person in self.people:
			if(((person.satisfied == person.preference_num) == False) & (min.satisfied > person.satisfied)):
				min = person

		return min.satisfied

def Satisfactions(file_name):
	tour = Tourism(0, 0, 0)

	file = open(file_name, "r")
	for line in file:
		if(line == '\n'):
			continue
		else:
			array = line.split('(')
			operation = array[0]
			for element in array:
				if(')' in element):
					num = element.split(')')[0]
					if(operation == 'people'):
						tour.people_num = int(num)
					elif(operation == 'places'):
						tour.location_num = int(num)
					elif(operation == 'preferences'):
						tour.max_prefs = int(num)
						tour.create_locations_people()
					elif(operation == 'place'):
						pref_attr = num.split(',')
						tour.add_location(int(pref_attr[0]), int(pref_attr[1]), int(pref_attr[2]), int(pref_attr[3]))
					elif(operation == 'prefer'):
						pref_attr = num.split(',')
						tour.update_person_pref(int(pref_attr[0]), int(pref_attr[1]))
					else:
						continue

	tour.allocate()
	min = tour.get_max_satisfaction()
	return min

file_name = sys.argv[1]
satisfactions = Satisfactions(file_name)
print("satisfaction(" + str(satisfactions) + ").")
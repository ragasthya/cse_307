(* 
	Student Name: Rahul S Agasthya
	Student ID	: 109961109
	
	CSE 307: HW3
	Tourism problem in SML.
*)

fun map(f, L) = if (L=[]) then []
	else f(hd(L))::(map(f, tl(L)));

fun find(L, E) = if (L=nil) then false
	else if (hd(L) = E) then true
	else find(tl(L), E);

fun myInterleave(x,[]) = [[x]]
	| myInterleave(x,h::t) = (x::h::t)::(map((fn l => h::l), myInterleave(x,t)));

fun appendAll(nil) = nil
	| appendAll(z::zs) = z @ (appendAll(zs));

fun permutations(nil) = [[]]
	| permutations(h::t) = appendAll(map((fn l => myInterleave(h, l)), permutations(t)));

fun populate_locations_helper(i, locations_num) = if (i=locations_num+1) then []
	else i::populate_locations_helper(i+1, locations_num);
	
fun populate_locations(locations) = if (locations=0) then []
	else populate_locations_helper(1, locations); 
	
fun person_pref(persons, preferences:(int * int * int) list):int list list =
	if preferences = [] then []
	else if persons = 0 then []
	else [#2(hd(preferences)), #3(hd(preferences))]::person_pref(persons, tl(preferences));
	
fun get_other_prefs_person(persons, current, above, preferences:(int * int * int) list):int list list = 
	if preferences = [] then []
	else if(#1(hd(preferences)) = current andalso #1(hd(tl(preferences))) = current andalso (#1(hd(preferences)) = #1(hd(tl(preferences)))) = false) then 
		[above, #3(hd(tl(preferences)))]::get_other_prefs_person(persons, current, above, tl(preferences))
	else get_other_prefs_person(persons, current, above, tl(preferences));

fun get_prefs_person(persons, current, preferences:(int * int * int) list):int list list =
	if preferences = [] then []
	else if(current = persons) then []
	else get_other_prefs_person(persons, current, #2(hd(preferences)), tl(preferences)) @ get_prefs_person(persons, current+1, tl(preferences));
	
fun get_other_prefs(persons, preferences:(int * int * int) list):int list list =
	if preferences = [] then []
	else if persons = 0 then []
	else get_prefs_person(persons, 1, preferences);
	
fun persons_preferences(persons, preferences:(int * int * int) list):int list list =
	person_pref(persons, preferences) @ get_other_prefs(persons, preferences);

fun is_violation(permutation, above, below) = 
	if(get_index(permutation, above) > get_index(permutation, below)) then true
	else false

fun get_violations(permutation:int list, prefs:int list list):int = 
	if(prefs = []) then 0
	else if is_violation(permutation, hd(hd(prefs)), hd(tl(hd(prefs)))) then 1+get_violations(permutation, tl(prefs))
	else get_violations(permutation, tl(prefs));
	
fun get_violations_permutations(permutations: int list list, prefs: int list list):int list =
	if(permutations = []) then []
	else get_violations_permutations(tl(permutations), prefs) @ [get_violations(hd(permutations), prefs)];

fun get_minimum_helper(violations, minimum) =
	if(violations = []) then minimum
	else if(hd(violations) < minimum) then get_minimum_helper(tl(violations), hd(violations))
	else get_minimum_helper(tl(violations), minimum);
	
fun get_minimum(violations) = 
	if(get_minimum_helper(violations, hd(violations)) = 0) then get_minimum_helper(violations, hd(violations))
	else get_minimum_helper(violations, hd(violations))+1;

fun violations(NumberOfPeople:int, NumberOfLocations:int, NumberOfPreferences:int, Preferences:(int * int * int) list): int = 
	if(NumberOfPeople = 0) then 0
	else if(NumberOfLocations = 0) then 0
	else get_minimum(get_violations_permutations(permutations(populate_locations(NumberOfLocations)), persons_preferences(NumberOfPeople, Preferences)));
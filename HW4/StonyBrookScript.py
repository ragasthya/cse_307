# -----------------------------------------------------------------------------
# Student Name: Rahul S Agasthya
# Student ID  : 109961109
# CSE 307: HW4 StonyBrookScript Part 1
# -----------------------------------------------------------------------------

tokens = (
    # 'NAME', 
    'NUMBER', 'FLOAT', 'STRING',
    'PLUS','MINUS','TIMES','DIVIDE', 'MODULUS', 'EXPONENT', 'FDIVIDE',
	'GREATER', 'LESSER', 'EQUIVALENCE', 'NOTEQUAL', 'GREATEREQUAL', 'LESSEREQUAL',
	'NOT', 'AND', 'OR', 
    'LPAREN','RPAREN', 'ALPAREN', 'ARPAREN', 'COMMA', 'IN'
    )

# Tokens

t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_MODULUS = r'/'
t_EXPONENT = r'\*\*'
t_FDIVIDE = r'//'
t_GREATER = r'>'
t_LESSER = r'<'
t_EQUIVALENCE = r'=='
t_NOTEQUAL = r'<>'
t_GREATEREQUAL = r'>='
t_LESSEREQUAL = r'<='
t_NOT = r'not'
t_AND = r'and'
t_OR = r'or'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_ALPAREN = r'\['
t_ARPAREN = r'\]'
t_COMMA = r','
t_IN = r'in'

def t_FLOAT(t):
    r'\d+\.\d+'
    try:
        t.value = float(t.value)
    except ValueError:
        print("Float value too large %d", t.value)
        t.value = 0
    return t

def t_NUMBER(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t

def t_STRING(t):
    r'"[^"]*"'
    t.value = t.value[1:-1]
    return t

# Ignored characters
t_ignore = " \t"

def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")
    
def t_error(t):
    print("ILLEGAL CHARACTER")
    t.lexer.skip(1)
    
# Build the lexer
import ply.lex as lex
lex.lex()

# Parsing rules
precedence = (
    ('left','PLUS','MINUS'),
    ('left','TIMES','DIVIDE', 'MODULUS'),
    ('left','EXPONENT','FDIVIDE'),
	('left', 'GREATER', 'LESSER', 'EQUIVALENCE', 'NOTEQUAL', 'GREATEREQUAL', 'LESSEREQUAL'),
	('left', 'NOT'), 
    ('left', 'AND'), 
    ('left', 'OR'), 
    )

def p_statement_expr(t):
    'statement : expression'
    print(t[1])

def p_expression_binop(t):
    '''expression : expression PLUS expression
                  | expression MINUS expression
                  | expression TIMES expression
                  | expression DIVIDE expression
                  | expression MODULUS expression
                  | expression EXPONENT expression
                  | expression FDIVIDE expression
				  | expression GREATER expression
                  | expression LESSER expression
                  | expression EQUIVALENCE expression
                  | expression NOTEQUAL expression
                  | expression GREATEREQUAL expression
                  | expression LESSEREQUAL expression
                  | NOT expression
                  | expression AND expression
                  | expression OR expression'''

    try:
        if (t[2] == '+'): 
            t[0] = t[1] + t[3]
        elif (t[2] == '-'): 
            t[0] = t[1] - t[3]
        elif (t[2] == '*'): 
            t[0] = t[1] * t[3]
        elif (t[2] == '/'): 
            t[0] = t[1] / t[3]
        elif (t[2] == '%'): 
            t[0] = t[1] % t[3]
        elif (t[2] == '**'): 
            t[0] = t[1] ** t[3]
        elif (t[2] == '//'): 
            t[0] = t[1] // t[3]
        elif (t[2] == '>'): 
            ret_type = t[1] > t[3]
            t[0] = 1 if(ret_type) else 0
        elif (t[2] == '<'): 
            ret_type = t[1] < t[3]
            t[0] = 1 if(ret_type) else 0
        elif (t[2] == '=='): 
            ret_type = t[1] == t[3]
            t[0] = 1 if(ret_type) else 0
        elif (t[2] == '<>'): 
            ret_type = not (t[1] == t[3])
            t[0] = 1 if(ret_type) else 0
        elif (t[2] == '>='): 
            ret_type = t[1] >= t[3]
            t[0] = 1 if(ret_type) else 0
        elif (t[2] == '<='): 
            ret_type = t[1] <= t[3]
            t[0] = 1 if(ret_type) else 0
        elif (t[1] == 'not'): 
            ret_type = not (t[2])
            t[0] = 1 if(ret_type) else 0
        elif (t[2] == 'and'): 
            ret_type = t[1] and t[3]
            t[0] = 1 if(ret_type) else 0
        elif (t[2] == 'or'):
            ret_type = t[1] or t[3]
            t[0] = 1 if(ret_type) else 0
    except:
        print("SEMANTIC ERROR")

def p_expression_group(t):
    'expression : LPAREN expression RPAREN'
    t[0] = t[2]

def p_expression_number(t):
    'expression : NUMBER'
    t[0] = t[1]

def p_expression_float(t):
    'expression : FLOAT'
    t[0] = t[1]

def p_expression_string(t):
    'expression : STRING'
    t[0] = t[1]

def p_expression_element(t):
    'expression : expression COMMA expression'
    element = [t[1]]
    if(type(t[3]) is list):
        element.extend(t[3])
    else:
        element.append(t[3])
    t[0] = element

def p_expression_list(t):
    'expression : ALPAREN expression ARPAREN'
    t[0] = t[2]

def p_expression_in(t):
    'expression : expression IN expression'
    try:
        t[0] = t[1] in t[3]
    except:
        print("SEMANNTIC ERROR")

def p_expression_indexRef(t):
    'expression : expression ALPAREN expression ARPAREN'
    try:
        t[0] = t[1][t[3]]
    except:
        print("SEMANNTIC ERROR")

def p_error(t):
    print("SYNTAX ERROR")

import ply.yacc as yacc
import sys
yacc.yacc()

if (len(sys.argv) != 2):
    sys.exit("INVALID ARGUEMENTS")
fd = open(sys.argv[1], 'r')

for line in fd:
    code = line.strip()
    try:
        if(code is ""):
            continue
    except EOFError:
        break
    yacc.parse(code)
import math
import sys
import copy

reserved = {
    'not': 'NOT',
    'and': 'AND',
    'or': 'OR',
    'print' : 'PRINT',
    'if' : 'IF',
    'else' : 'ELSE',
    'while' : 'WHILE',
    'return': 'RETURN'
}

tokens = [
    'INTEGER', 'REAL', 'STRING',
    'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'EQUALS', 'MODULUS',
    'POWER', 'FLOOR', 'LESS', 'LESSEQUAL', 'EQUALTO', 'NOTEQUALTO',
    'GREATER','GREATEREQUAL', 'NOT', 'AND', 'OR',
    'LPAREN','RPAREN', 'COMMA', 'LBRACKET', 'RBRACKET', 'NAME', 'IN',
    'PRINT', 'SEMICOLON', 'LBRACE', 'RBRACE'
    ]+ list(reserved.values())

#Tokens
t_PLUS   = r'\+'
t_MINUS  = r'-'
t_TIMES  = r'\*'
t_DIVIDE = r'/'
t_EQUALS = r'='
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACKET = r'\['
t_RBRACKET = r'\]'
t_MODULUS = r'%'
t_POWER = r'\*\*'
t_FLOOR = r'//'
t_LESS = r'<'
t_LESSEQUAL = r'<='
t_EQUALTO = r'=='
t_NOTEQUALTO = r'<>'
t_GREATER = r'>'
t_GREATEREQUAL = r'>='
t_COMMA = r','
t_SEMICOLON = r';'
t_LBRACE = r'{'
t_RBRACE = r'}'

#Ignore spaces and tabs
t_ignore = ' \t'


def t_NAME(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'NAME')    # Check for reserved words
    return t

#Real rule
def t_REAL(t):
    r'\d*\.\d*'
    t.value = float (t.value)
    return t

#Integer rule
def t_INTEGER(t):
    r'\d+'
    t.value = int (t.value)
    return t

#Real rule
def t_STRING(t):
    r'"[^"]*"'
    t.value = t.value[1:-1]
    return t

# Rule to track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


#Invalid token rule
def t_error(t):
    #print("SYNTAX ERROR")
    t.lexer.skip(1)

#Build the lexer
import ply.lex as lex
lexer = lex.lex()

# Test it out
data = '''
return 4;
'''

# Give the lexer some input
lexer.input(data)

# Tokenize
while True:
    tok = lexer.token()
    if not tok: 
        break      # No more input
    print(tok)




# Parsing rules
precedence = (
    ('left', 'OR'),
    ('left', 'AND'),
    ('left', 'NOT'),
    ('left', 'LESS', 'LESSEQUAL', 'EQUALTO', 'NOTEQUALTO','GREATER','GREATEREQUAL'),
    ('left', 'IN'),
    ('left','PLUS','MINUS'),
    ('left', 'FLOOR'),
    ('left', 'POWER'),
    ('left', 'MODULUS'),
    ('left', 'TIMES','DIVIDE'),
    ('left', 'LBRACKET', 'RBRACKET'),
    ('right','UMINUS'),
    ('right', 'EQUALS')
    )

# dictionary of names
names = [{ }]

global functionParameters
functionParameters = []
global functionStatements
functionStatements = None

class Node:
    def __init__(self,value):
        self.value = value
    def returnNameValue(self):
        return self.value
    def execute(self):
        return self.value

class IntegerNode(Node):
    def __init__(self,value):
        self.value = value
    def execute(self):
        return self.value

class RealNode(Node):
    def __init__(self,value):
        self.value = value
    def execute(self):
        return self.value

class StringNode(Node):
    def __init__(self,value):
        self.value = value
    def execute(self):
        return self.value
    
class PrintNode(Node):
    def __init__(self, value):
        self.value = value
    def execute(self):
        print(self.value.execute())

class BinopNode(Node):
    def __init__(self, left, op, right):
        self.left = left
        self.op= op
        self.right = right
    def execute(self):
        try:
            if self.op == '+' : return self.left.execute() + self.right.execute()
            elif self.op == '-': return self.left.execute() - self.right.execute()
            elif self.op == '*': return self.left.execute() * self.right.execute()
            elif self.op == '/': return self.left.execute() / self.right.execute()
            elif self.op == '%': return self.left.execute() % self.right.execute()
            elif self.op == '**': return math.pow(self.left.execute(),self.right.execute())
            elif self.op == '//': return math.floor(self.left.execute()/self.right.execute())
        except (TypeError, ZeroDivisionError):
            print("SEMANTIC ERROR")

class ComparisonNode(Node):
    def __init__(self, left, op, right):
        self.left = left
        self.op= op
        self.right = right
    def execute(self):
        try:
            if self.op   == '<': return 1 if (self.left.execute() < self.right.execute()) else 0
            elif self.op == '<=': return 1 if (self.left.execute() <= self.right.execute()) else 0
            elif self.op == '==': return 1 if (self.left.execute() == self.right.execute()) else 0
            elif self.op == '<>': return 1 if (self.left.execute() != self.right.execute()) else 0
            elif self.op == '>': return 1 if (self.left.execute() > self.right.execute()) else 0
            elif self.op == '>=': return 1 if (self.left.execute() >= self.right.execute()) else 0
        except (TypeError, ZeroDivisionError):
            print("SEMANTIC ERROR")

class AndOrNode(Node):
    def __init__(self, left, op, right):
        self.left = left
        self.op = op
        self.right = right
    def execute(self):
        try:
            if self.op == 'and': return 1  if(self.left.execute() and self.right.execute()) else 0
            elif self.op == 'or': return 1 if(self.left.execute() or self.right.execute()) else 0
        except (TypeError):
            print("SEMANTIC ERROR")

class NotNode(Node):
    def __init__(self, op, right):
        self.op = op
        self.right = right
    def execute(self):
        try:
            return 1 if (not self.right.execute()) else 0
        except (TypeError):
            print("SEMANTIC ERROR")

class IndexNode(Node):
    def __init__(self,a,b):
        self.a = a
        self.b = b
    def execute(self):
        try:
            return self.a.execute()[self.b.execute()]
        except (TypeError, IndexError):
            print("SEMANTIC ERROR")
    
class InNode(Node):
    def __init__(self,a,b):
        self.a = a
        self.b = b
    def execute(self):
        return 1 if self.a.execute() in self.b.execute() else 0

class AssignNode(Node):
    def __init__(self,left, index, right):
        self.left = left
        self.index = index
        self.right = right
    def execute(self):
        if self.index == None:
            names[0][self.left] = self.right.execute()
        else:
            names[0][self.left][self.index.execute()] = self.right.execute()
        return names

class NameNode(Node):
    def __init__(self, value):
        self.value = value
    def returnNameValue(self):
        return self.value
    def execute(self):
        if self.value not in names:
            return self.returnNameValue()
        else:
            return names[0][self.value]

class ListBodyNode(Node):
    def __init__(self,element1,element2):
        self.element1 = element1
        self.element2 = element2
    def execute(self):
        resultList = [self.element1.execute()]
        resultList.extend(self.element2.execute()) if type(self.element2.execute()) is list else resultList.append(self.element2.execute())
        return resultList

class BlockNode(Node):
    def __init__(self, sl):
        self.statementNodes = sl
    def execute(self):
        return self.statementNodes.execute()

class StatementListNode(Node):
    def __init__(self, statement):
        self.statement = statement
    def execute(self):
        result = None
        for statement in self.statement:
            temp = statement.execute() 
            if temp != None:
                result = temp
        return result

 
class IfNode(Node):
    def __init__(self, c, t, e):
        self.condition = c
        self.thenBlock = t
        self.elseBlock= e
    def execute(self):
        if(self.condition.execute()):
            self.thenBlock.execute()
        else:
            if self.elseBlock != None:
                self.elseBlock.execute()

class WhileNode(Node):
    def __init__(self, c, t):
        self.condition = c
        self.thenBlock = t
    def execute(self):
        while(self.condition.execute()):
            self.thenBlock.execute()

class FunctionNode(Node):
    def __init__(self, parameters, statements, block):
        self.parameters = parameters
        self.statements = statements
        self.block = block
    def execute(self):
        global functionParameters
        print(self.parameters)
        functionParameters = self.parameters
        global functionStatements
        functionStatements= self.statements
        print(functionStatements)
        return self.block.execute()
        
class FunctionCallNode(Node):
    def __init__(self,parameters):
        self.parameters = parameters
    def execute(self):
        tempNames = copy.deepcopy(names[0])
        names.insert(0,tempNames)
        global functionParameters
        i = 0
        #listvalues = self.parameters[0].execute()
        #listnames = functionParameters[i].execute()
        for parameter in self.parameters:
            names[0][functionParameters[i].returnNameValue()] = parameter.execute()
            i = i + 1
        global functionStatements
        print(names)
        result = functionStatements[0].execute()
        names.pop(0)
        return result

class ReturnNode(Node):
    def __init__(self,value):
        self.value = value
    def execute(self):
        return self.value.execute()
       
def p_statement_function(t):
    'statement : NAME LPAREN expression RPAREN statement LBRACE block RBRACE'
    t[0] = FunctionNode([t[3]], [t[5]], t[7])

def p_statement_block(t):
    'statement : LBRACE block RBRACE '
    t[0] = BlockNode(t[2])

def p_block_statement_list(t):
    'block : statement block'
    t[0] = StatementListNode([t[1], t[2]])

def p_block_statment(t):
    'block : statement'
    t[0] = t[1]

def p_statement_print(t):
    'statement : PRINT LPAREN expression RPAREN SEMICOLON'
    t[0] = PrintNode(t[3])

def p_statement_assign1(t):
    'statement : NAME LBRACKET expression RBRACKET EQUALS expression SEMICOLON'
    t[0] = AssignNode(t[1],t[3],t[6])   

def p_statement_assign(t):
    'statement : NAME EQUALS expression SEMICOLON'
    t[0] = AssignNode(t[1],None,t[3])

def p_statement_if(t):
    'statement : IF LPAREN expression RPAREN statement ELSE statement'
    t[0] = IfNode(t[3],t[5],t[7])

def p_statement_else(t):
    'statement : IF LPAREN expression RPAREN statement'
    t[0] = IfNode(t[3],t[5], None)

def p_statement_while(t):
    'statement : WHILE LPAREN expression RPAREN statement'
    t[0] = WhileNode(t[3],t[5])

def p_expression_return(t):
    'statement : RETURN expression SEMICOLON'
    t[0] = ReturnNode(t[2])

def p_expression_integer(t):
    'expression : INTEGER'
    t[0] = IntegerNode(t[1])

def p_expression_real(t):
    'expression : REAL'
    t[0] = RealNode(t[1])

def p_expression_string(t):
    'expression : STRING'
    t[0] = StringNode(t[1])

def p_expression_binop(t):
    '''expression : expression PLUS expression
                 | expression MINUS expression
                 | expression TIMES expression
                 | expression DIVIDE expression
                 | expression MODULUS expression
                 | expression POWER expression
                 | expression FLOOR expression'''
    t[0] = BinopNode(t[1],t[2],t[3])
        
def p_expression_comparison(t):
    '''expression : expression LESS expression
                 | expression LESSEQUAL expression
                 | expression EQUALTO expression
                 | expression NOTEQUALTO expression
                 | expression GREATER expression
                 | expression GREATEREQUAL expression'''
    t[0] = ComparisonNode(t[1], t[2], t[3])

def p_expression_andornot(t):
    '''expression : NOT expression
                 | expression AND expression
                 | expression OR expression'''
    if t[1] == 'not':
        t[0] = NotNode(t[1], t[2])
    else:
        t[0] = AndOrNode(t[1], t[2],t[3])
        
def p_expression_uminus(t):
    'expression : MINUS expression %prec UMINUS'
    t[0] = -t[2]

def p_expression_name(t):
    'expression : NAME'
    t[0] = NameNode(t[1])

def p_expression_group(t):
    'expression : LPAREN expression RPAREN'
    t[0] = t[2]

def p_expression_list(t):
    'expression : LBRACKET expression RBRACKET'
    t[0] = t[2]

def p_expression_listbody(t):
    'expression : expression COMMA expression'
    t[0] = ListBodyNode(t[1],t[3])

def p_expression_index(t):
    'expression : expression LBRACKET expression RBRACKET'
    t[0] = IndexNode(t[1],t[3])

def p_expression_in(t):
    'expression : expression IN expression'
    t[0] = InNode(t[1],t[3])

def p_expression_functionCall(t):
    'expression : NAME LPAREN expression RPAREN'
    t[0] = FunctionCallNode([t[3]])


def p_error(t):
    print("SYNTAX ERROR", t.value)

import ply.yacc as yacc
parser = yacc.yacc()

inputFile = open(sys.argv[1])
input = inputFile.read()
ast = parser.parse(input)
ast.execute()












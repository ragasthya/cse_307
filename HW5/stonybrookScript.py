import math
import sys
import copy

toggled = {
    'not': 'NOT',
    'and': 'AND',
    'or': 'OR',
    'print' : 'PRINT',
    'if' : 'IF',
    'else' : 'ELSE',
    'while' : 'WHILE',
    'return': 'RETURN'
}

tokens = [
    'INTEGER', 'FLOAT', 'STRING',
    'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'EQUALS', 'MODULUS',
    'POWER', 'FLOOR', 'LESS', 'LESSEQUAL', 'EQUALTO', 'NOTEQUALTO',
    'GREATER','GREATEREQUAL', 'NOT', 'AND', 'OR',
    'LPAREN','RPAREN', 'COMMA', 'LBRACKET', 'RBRACKET', 'NAME', 'IN',
    'PRINT', 'SEMICOLON', 'LBRACE', 'RBRACE',
    'IF', 'ELSE', 'WHILE', 'RETURN'
    ]

#Tokens
t_PLUS   = r'\+'
t_MINUS  = r'-'
t_TIMES  = r'\*'
t_DIVIDE = r'/'
t_EQUALS = r'='
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACKET = r'\['
t_RBRACKET = r'\]'
t_MODULUS = r'%'
t_POWER = r'\*\*'
t_FLOOR = r'//'
t_LESS = r'<'
t_LESSEQUAL = r'<='
t_EQUALTO = r'=='
t_NOTEQUALTO = r'<>'
t_GREATER = r'>'
t_GREATEREQUAL = r'>='
t_COMMA = r','
t_SEMICOLON = r';'
t_LBRACE = r'{'
t_RBRACE = r'}'

#Ignore spaces and tabs
t_ignore = ' \t'

#Name of variable
def t_NAME(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = toggled.get(t.value,'NAME')
    return t

#Variable is a Float
def t_FLOAT(t):
    r'\d*\.\d*'
    t.value = float (t.value)
    return t

#Variable is a Integer
def t_INTEGER(t):
    r'\d+'
    t.value = int (t.value)
    return t

#Variable is a String
def t_STRING(t):
    r'"[^"]*"'
    t.value = t.value[1:-1]
    return t

#New Line and Invalids
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def t_error(t):
    t.lexer.skip(1)

#LEXER
import ply.lex as lex
lexer = lex.lex()

#PARSER RULES AND PRECEDENCE
precedence = (
    ('left', 'OR'),
    ('left', 'AND'),
    ('left', 'NOT'),
    ('left', 'LESS', 'LESSEQUAL', 'EQUALTO', 'NOTEQUALTO', 'GREATER', 'GREATEREQUAL'),
    ('left', 'IN'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'FLOOR'),
    ('left', 'POWER'),
    ('left', 'MODULUS'),
    ('left', 'TIMES', 'DIVIDE'),
    ('left', 'LBRACKET', 'RBRACKET'),
    ('right','UMINUS'),
    ('right', 'EQUALS')
    )

#NAMES OF VARIABLES DICTIONARY
names = [{ }]

class Node:
    def __init__(self,value):
        self.value = value
    def returnNameValue(self):
        return self.value
    def execute(self):
        return self.value

class IntegerNode(Node):
    def __init__(self,value):
        self.value = value
    def execute(self):
        return self.value

class FloatNode(Node):
    def __init__(self,value):
        self.value = value
    def execute(self):
        return self.value

class StringNode(Node):
    def __init__(self,value):
        self.value = value
    def execute(self):
        return self.value
    
class PrintNode(Node):
    def __init__(self, value):
        self.value = value
    def execute(self):
        print(self.value.execute())

class OperationNode(Node):
    def __init__(self, left, op, right):
        self.left = left
        self.op= op
        self.right = right
    def execute(self):
        try:
            if self.op == '+' : 
                return self.left.execute() + self.right.execute()
            elif self.op == '-': 
                return self.left.execute() - self.right.execute()
            elif self.op == '*': 
                return self.left.execute() * self.right.execute()
            elif self.op == '/': 
                return self.left.execute() / self.right.execute()
            elif self.op == '%': 
                return self.left.execute() % self.right.execute()
            elif self.op == '**': 
                return self.left.execute() ** self.right.execute()
            elif self.op == '//': 
                return self.left.execute() // self.right.execute()
            elif self.op == '<': 
                if (self.left.execute() < self.right.execute()):
                    return 1
                else:
                    return 0
            elif self.op == '<=':
                if (self.left.execute() <= self.right.execute()):
                    return 1
                else:
                    return 0
            elif self.op == '==':
                if (self.left.execute() == self.right.execute()):
                    return 1
                else:
                    return 0
            elif self.op == '<>': 
                if not (self.left.execute() == self.right.execute()):
                    return 1
                else:
                    return 0
            elif self.op == '>':
                if (self.left.execute() > self.right.execute()):
                    return 1
                else:
                    return 0
            elif self.op == '>=':
                if (self.left.execute() >= self.right.execute()):
                    return 1
                else:
                    return 0
        except (TypeError, ZeroDivisionError):
            print("SEMANTIC ERROR1")

class LogicalNode(Node):
    def __init__(self, left, op, right):
        self.left = left
        self.op = op
        self.right = right
    def execute(self):
        if self.op == 'and': 
            if (self.left.execute() and self.right.execute()):
                return 1
            else:
                return 0
        elif self.op == 'or':
            if (self.left.execute() or self.right.execute()):
                return 1
            else:
                return 0

class LogicalNotNode(Node):
    def __init__(self, op, right):
        self.op = op
        self.right = right
    def execute(self):
        if (not self.right.execute()):
            return 1
        else:
            return 0

class IndexNode(Node):
    def __init__(self,a,b):
        self.a = a
        self.b = b
    def execute(self):
        return self.a.execute()[self.b.execute()]
    
class InNode(Node):
    def __init__(self,a,b):
        self.a = a
        self.b = b
    def execute(self):
        return 1 if self.a.execute() in self.b.execute() else 0

class AssignNode(Node):
    def __init__(self,left, index, right):
        self.left = left
        self.index = index
        self.right = right
    def execute(self):
        if self.index == None:
            names[0][self.left] = self.right.execute()
        else:
            names[0][self.left][self.index.execute()] = self.right.execute()
        return names

class NameNode(Node):
    def __init__(self, value):
        self.value = value
    def execute(self):
        return names[0][self.value]

class ListBodyNode(Node):
    def __init__(self,element1,element2):
        self.element1 = element1
        self.element2 = element2
    def execute(self):
        resultList = [self.element1.execute()]
        resultList.extend(self.element2.execute()) if type(self.element2.execute()) is list else resultList.append(self.element2.execute())
        return resultList

class BlockNode(Node):
    def __init__(self, sl):
        self.statementNodes = sl
    def execute(self):
        result = None
        temp = self.statementNodes.execute()
        if temp != None:
            result = temp
        return result

class StatementListNode(Node):
    def __init__(self, statement):
        self.statement = statement
    def execute(self):
        result = None
        for statement in self.statement:
            temp = statement.execute() 
            if temp != None:
                result = temp
        return result

 
class IfElseNode(Node):
    def __init__(self, c, t, e):
        self.condition = c
        self.thenBlock = t
        self.elseBlock= e
    def execute(self):
        if(self.condition.execute()):
            result = self.thenBlock.execute()
            return result
        else:
            if self.elseBlock != None:
                result = self.elseBlock.execute()
                return result

class IfNode(Node):
    def __init__(self, c, t):
        self.condition = c
        self.thenBlock = t
    def execute(self):
        if(self.condition.execute()):
            result = self.thenBlock.execute()
            return result

class LoopNode(Node):
    def __init__(self, c, b):
        self.condition = c
        self.block = b
    def execute(self):
        while(self.condition.execute()):
            self.block.execute()

def p_statement_block(t):
    'statement : LBRACE block RBRACE '
    t[0] = BlockNode(t[2])

def p_block_statement_list(t):
    'block : statement block'
    t[0] = StatementListNode([t[1], t[2]])

def p_block_statment(t):
    'block : statement'
    t[0] = t[1]

def p_statement_print(t):
    'statement : PRINT LPAREN expression RPAREN SEMICOLON'
    t[0] = PrintNode(t[3])

def p_statement_assign1(t):
    'statement : NAME LBRACKET expression RBRACKET EQUALS expression SEMICOLON'
    t[0] = AssignNode(t[1],t[3],t[6])   

def p_statement_assign(t):
    'statement : NAME EQUALS expression SEMICOLON'
    t[0] = AssignNode(t[1],None,t[3])

def p_statement_if_else(t):
    'statement : IF LPAREN expression RPAREN statement ELSE statement'
    t[0] = IfElseNode(t[3],t[5],t[7])

def p_statement_if(t):
    'statement : IF LPAREN expression RPAREN statement'
    t[0] = IfNode(t[3],t[5])

def p_statement_while(t):
    'statement : WHILE LPAREN expression RPAREN statement'
    t[0] = LoopNode(t[3],t[5])

def p_expression_return(t):
    'statement : RETURN expression SEMICOLON'
    t[0] = ReturnNode(t[2])

def p_expression_integer(t):
    'expression : INTEGER'
    t[0] = IntegerNode(t[1])

def p_expression_float(t):
    'expression : FLOAT'
    t[0] = FloatNode(t[1])

def p_expression_string(t):
    'expression : STRING'
    t[0] = StringNode(t[1])

def p_expression_operation(t):
    '''expression : expression PLUS expression
                 | expression MINUS expression
                 | expression TIMES expression
                 | expression DIVIDE expression
                 | expression MODULUS expression
                 | expression POWER expression
                 | expression FLOOR expression
                 | expression LESS expression
                 | expression LESSEQUAL expression
                 | expression EQUALTO expression
                 | expression NOTEQUALTO expression
                 | expression GREATER expression
                 | expression GREATEREQUAL expression'''
    t[0] = OperationNode(t[1],t[2],t[3])

def p_expression_logical(t):
    '''expression : NOT expression
                 | expression AND expression
                 | expression OR expression'''
    if t[1] == 'not':
        t[0] = LogicalNotNode(t[1], t[2])
    else:
        t[0] = LogicalNode(t[1], t[2],t[3])
        
def p_expression_uminus(t):
    'expression : MINUS expression %prec UMINUS'
    t[0] = -t[2]

def p_expression_name(t):
    'expression : NAME'
    t[0] = NameNode(t[1])

def p_expression_group(t):
    'expression : LPAREN expression RPAREN'
    t[0] = t[2]

def p_expression_list(t):
    'expression : LBRACKET expression RBRACKET'
    t[0] = t[2]

def p_expression_listbody(t):
    'expression : expression COMMA expression'
    t[0] = ListBodyNode(t[1],t[3])

def p_expression_index(t):
    'expression : expression LBRACKET expression RBRACKET'
    t[0] = IndexNode(t[1],t[3])

def p_expression_in(t):
    'expression : expression IN expression'
    t[0] = InNode(t[1],t[3])

def p_error(t):
    print("SYNTAX ERROR", t.value)

import ply.yacc as yacc
parser = yacc.yacc()

inputFile = open(sys.argv[1])
input = inputFile.read()
ast = parser.parse(input)
ast.execute()
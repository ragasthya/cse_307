def average(file_name):
	lines = []
	with open(file_name) as f:
		lines = f.readlines()

	sum = 0
	for line in lines:
		line.strip()
		sum = sum + float(line)

	return sum/len(lines)

def get_average(list):
	length = len(list)
	if len(list) == 0:
		return 0
	return (list[0] + (get_average(list[1:])*(length - 1)))/length

def average_rec(file_name):
	lines = []
	with open(file_name) as f:
		lines = f.readlines()

	lines = list(map(float, lines))

	print (str(get_average(lines)))


average_rec('numbers.txt')
print (str(average('numbers.txt')))
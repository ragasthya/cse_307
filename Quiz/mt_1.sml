fun reverse_helper(L'a list, A'a list) = if L = [] then A
	else reverse_helper(tl(L), hd(L)::A);

fun reverse(L'a list) = if L=[] then []
	else reverse_helper(L, []);
	
fun is_palindrome(L:'a list) =
	L = reverse(L);
def is_prime_number (number):
	if (number < 2):
		print("Input not valid.")
		return False
	i = 2
	while (i < number):
		if(number%i == 0):
			return False
		i = i + 1
	return True

def is_prime (number, divisor):
	if(divisor >= number): 
		return True
	if(number%divisor == 0): 
		return False
	return is_prime(number, divisor+1)

def check_prime(number):
	if (number < 2):
		print("Input not valid.")
		return False
	return is_prime(number, 2)

print (is_prime_number(5))
print (is_prime_number(25))
print (is_prime_number(331))
print (is_prime_number(7492))
print (is_prime_number(7491))
print ("--------------------")
print (check_prime(-2))
print (check_prime(-25))
print (check_prime(2))
print (check_prime(25))
print (check_prime(331))
print (check_prime(7492))
print (check_prime(7491))